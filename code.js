// This plugin will open a window to prompt the user to enter a number, and
// it will then create that many rectangles on the screen.
// This file holds the main code for the plugins. It has access to the *document*.
// You can access browser APIs in the <script> tag inside "ui.html" which has a
// full browser environment (see documentation).
// This shows the HTML page in "ui.html".
figma.showUI(__html__, {
    height: 400,
    width: 300
});
// Calls to "parent.postMessage" from within the HTML page will trigger this
// callback. The callback will be passed the "pluginMessage" property of the
// posted message.
figma.ui.onmessage = msg => {
    // One way of distinguishing between different types of messages sent from
    // your HTML page is to use an object with a "type" property like this.
    if (msg.type === 'zoom-to-real') {
        var realWidth = 0;
        var pxCount = 0;
        var targetRealWidth = 0;
        var targetPxCount = 0;
        var setupComplete = true;
        pxCount = msg.screenWidthPx;
        realWidth = msg.screenWidthCm;
        targetRealWidth = msg.targetWidthCm;
        targetPxCount = msg.targetWidthPx;
    }
    if (setupComplete) {
        console.log(setupComplete);
        var cmPerPx = realWidth / pxCount;
        var requiredPx = targetRealWidth / cmPerPx;
        var zoomit = requiredPx / targetPxCount;
        console.log(zoomit, cmPerPx);
    }
    figma.viewport.zoom = zoomit;
    console.log(zoomit);
    // Make sure to close the plugin when you're done. Otherwise the plugin will
    // keep running, which shows the cancel button at the bottom of the screen.
    figma.closePlugin();
};
